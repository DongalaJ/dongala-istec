# PIII e PCIII #

## Aulas ##

[Intrudução](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/intruducao.pdf)

[Revisões](https://bitbucket.org/GoncaloF/istecp316/raw/74afa7f612e500a99491606244cb0a66a560563a/slides/Aula1.pdf)

[Playground](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/aula2.pdf)

[Swift intrudução](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/slides/aula3.pdf)



## Fichas de trabalho ##
[Ficha 1](https://bitbucket.org/GoncaloF/istecp316/raw/3459022dc6a95f788d8893d1c6469399c1c66986/fichas%20de%20trabalho/ficha1.pdf)

[Ficha 2](https://bitbucket.org/GoncaloF/istecp316/raw/b2e801b74743d1d1a40a5d19f4dbcc40b67330fe/fichas%20de%20trabalho/ficha2.pdf)


### Soluções das fichas ###